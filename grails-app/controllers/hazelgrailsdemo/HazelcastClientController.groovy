package hazelgrailsdemo

class HazelcastClientController {

    HazelcastClientService hazelcastClientService

    def index() {
        int count = 0
        if (hazelcastClientService.isCloudClientRunning()) {
            count = hazelcastClientService.getEmployeesCount()
        }
        render(view: 'index', model: [count: count])
    }

    def startLocalClient() {
        try {
            hazelcastClientService.startLocalClient()
            flash.message = "Local client started"
        } catch (Exception ex) {
            flash.error = ex.getMessage()
        }
        redirect(action: 'index')
    }

    def stopLocalClient() {
        if (hazelcastClientService.isLocalClientRunning()) {
            hazelcastClientService.stopLocalClient()
            flash.message  = "Local client stopped"
        } else {
            flash.error  = "No local client running"
        }
        redirect(action: 'index')
    }

    def startCloudClient() {
        try {
            hazelcastClientService.startCloudClient()
            flash.message = "Cloud client started"
        } catch (Exception ex) {
            flash.error  = ex.getMessage()
        }
        redirect(action: 'index')
    }

    def stopCloudClient() {
        if (hazelcastClientService.isCloudClientRunning()) {
            hazelcastClientService.stopCloudClient()
            flash.message = "Cloud Client stopped"
        } else {
            flash.error = "No Cloud Client running"
        }
        redirect(action: 'index')
    }

    def createEmployees() {
        hazelcastClientService.cacheEmployees()
        flash.message = "Employees created successfully"
        redirect(action: 'index')
    }
}
