package hazelgrailsdemo

import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IMap
import demo.hazelcast.HazelcastClientFactory
import grails.gorm.transactions.Transactional

@Transactional
class HazelcastClientService {

    def startLocalClient() {
        HazelcastInstance client = HazelcastClientFactory.getLocalClient()
        if (client.getLifecycleService().isRunning()) {
            log.info("Local client ${client.name} started successfully")
        } else {
            log.info("Failed to start Local client")
        }
    }

    def stopLocalClient() {
        if (HazelcastClientFactory.isLocalClientRunning()) {
            HazelcastClientFactory.stopLocalClient()
            log.info("Local Client stopped successfully")
        } else {
            log.info("No Local Client is running")
        }
    }

    def startCloudClient() {
        HazelcastInstance client = HazelcastClientFactory.getCloudClient()
        if (client.getLifecycleService().isRunning()) {
            log.info("Cloud client ${client.name} started successfully")
        } else {
            log.info("Failed to start Cloud client")
        }
    }

    def stopCloudClient() {
        if (HazelcastClientFactory.isCloudClientRunning()) {
            HazelcastClientFactory.stopCloudClient()
            log.info("Cloud Client stopped successfully")
        } else {
            log.info("No Cloud Client is running")
        }
    }

    boolean isLocalClientRunning() {
        return HazelcastClientFactory.isLocalClientRunning()
    }

    boolean isCloudClientRunning() {
        return HazelcastClientFactory.isCloudClientRunning()
    }

    HazelcastInstance getLocalClient() {
        return HazelcastClientFactory.getLocalClient()
    }

    HazelcastInstance getCloudClient() {
        return HazelcastClientFactory.getCloudClient()
    }

    def cacheEmployees() {
        HazelcastInstance client = getCloudClient()
        IMap<String, Object> employeesMap = client.getMap("employeesMap")
        10.times { n ->
            String empKey = "emp" + System.currentTimeMillis()
            employeesMap.put(empKey, new Employee(name: "Employee-$n", designation: "Software Engineer-${System.currentTimeMillis()}"))
        }
    }

    def getEmployeesCount() {
        HazelcastInstance client = getCloudClient()
        return client.getMap("employeesMap").size()
    }
}
