<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>
    <content tag="nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Application Status <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Environment: ${grails.util.Environment.current.name}</a></li>
                <li><a href="#">App profile: ${grailsApplication.config.grails?.profile}</a></li>
                <li><a href="#">App version:
                    <g:meta name="info.app.version"/></a>
                </li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Grails version:
                    <g:meta name="info.app.grailsVersion"/></a>
                </li>
                <li><a href="#">Groovy version: ${GroovySystem.getVersion()}</a></li>
                <li><a href="#">JVM version: ${System.getProperty('java.version')}</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Artefacts <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Controllers: ${grailsApplication.controllerClasses.size()}</a></li>
                <li><a href="#">Domains: ${grailsApplication.domainClasses.size()}</a></li>
                <li><a href="#">Services: ${grailsApplication.serviceClasses.size()}</a></li>
                <li><a href="#">Tag Libraries: ${grailsApplication.tagLibClasses.size()}</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Installed Plugins <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">
                    <li><a href="#">${plugin.name} - ${plugin.version}</a></li>
                </g:each>
            </ul>
        </li>
    </content>

    %{--<div class="svg" role="presentation">
        <div class="grails-logo-container">
            <asset:image src="grails-cupsonly-logo-white.svg" class="grails-logo"/>
        </div>
    </div>--}%

    <div id="content" role="main">
        <section class="row colset-2-its">
            <g:if test="${count > 0}">
                <h1>Cached Employees Count = ${count}</h1>
            </g:if>
            <g:else>
                <h1>Welcome to Hazelcast Cloud</h1>
            </g:else>
            <div>
                <g:if test="${flash.message}">
                    <div class="message">${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                    <div class="errors">${flash.error}</div>
                </g:if>
            </div>

            <div style="margin-left:22px;">
                <ul style="list-style: none">
%{--                    <li> <g:link controller="hazelcastClient" action="startLocalClient">Start Local Client</g:link> </li>--}%
%{--                    <li> <g:link controller="hazelcastClient" action="stopLocalClient">Stop Local Client</g:link> </li>--}%
                    <li> <g:link controller="hazelcastClient" action="startCloudClient"><h1>Start Cloud Client</h1></g:link> </li>
                    <li> <g:link controller="hazelcastClient" action="stopCloudClient"><h1>Stop Cloud Client</h1></g:link> </li>
                    <li> <g:link controller="hazelcastClient" action="createEmployees"><h1>Create Employees</h1></g:link> </li>
                </ul>
            </div>
        </section>
    </div>

</body>
</html>
