package demo.hazelcast

import com.hazelcast.client.HazelcastClient
import com.hazelcast.client.spi.impl.discovery.HazelcastCloudDiscovery
import com.hazelcast.client.spi.properties.ClientProperty
import com.hazelcast.config.GroupConfig
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.client.config.ClientConfig

class HazelcastClientFactory {

    private static HazelcastInstance localClient
    private static HazelcastInstance cloudClient

    static HazelcastInstance getLocalClient() {

        if (!(localClient != null && localClient.getLifecycleService().isRunning())) {
            ClientConfig config = new ClientConfig()
            config.getNetworkConfig().addAddress("192.168.8.115:5701")
            localClient = HazelcastClient.newHazelcastClient(config)
        }
        return localClient
    }

    static HazelcastInstance getCloudClient() {
        if (!(cloudClient != null && cloudClient.getLifecycleService().isRunning())) {
            ClientConfig config = new ClientConfig()
            config.setGroupConfig(new GroupConfig("demo-cluster", "9f0b2fcd1be147bf968a6ea06c7aca90"));
            config.setProperty("hazelcast.client.statistics.enabled","true");
            config.setProperty(ClientProperty.HAZELCAST_CLOUD_DISCOVERY_TOKEN.getName(), "Lzq38N3re200mPEujvyXYEkmdfUcBj7LIZEphjh8zaQzMjgB4i");
            config.setProperty(HazelcastCloudDiscovery.CLOUD_URL_BASE_PROPERTY.getName(), "https://coordinator.hazelcast.cloud");
            cloudClient = HazelcastClient.newHazelcastClient(config)
        }
        return cloudClient
    }

    static isLocalClientRunning() {
        return localClient != null && localClient.getLifecycleService().isRunning()
    }

    static isCloudClientRunning() {
        return cloudClient != null && cloudClient.getLifecycleService().isRunning()
    }

    static stopLocalClient() {
        localClient.shutdown()
    }

    static stopCloudClient() {
        cloudClient.shutdown()
    }
}
